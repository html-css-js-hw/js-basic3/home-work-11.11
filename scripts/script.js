/*
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

// Task 1

function addParagraph() {
    const newParagraph = document.createElement("p");
    newParagraph.textContent = "New Paragraph";
    const contentSection = document.getElementById("content");
    contentSection.appendChild(newParagraph);
}

const clickButton = document.createElement("button");
clickButton.id = "btn-click";
clickButton.textContent = "Click Me";
clickButton.addEventListener("click", addParagraph);

const contentSection = document.getElementById("content");
contentSection.appendChild(clickButton);

//Task 2
function addInput() {
    const newInput = document.createElement("input");
    newInput.type = "text";
    newInput.placeholder = "Enter something";
    newInput.name = "customInput";

    const contentSection = document.getElementById("content");

    const createButton = document.createElement("button");
    createButton.id = "btn-input-create";
    createButton.textContent = "Create Input";
    createButton.addEventListener("click", function () {
        contentSection.insertBefore(newInput, createButton.nextSibling);
    });
    contentSection.appendChild(createButton);
}
addInput();